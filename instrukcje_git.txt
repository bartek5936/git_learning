GIT INIT
- git init <- tworzy nowe repozytorium

GIT CLONE
- git clone -b <branchname> <remote-repo-url>
- git clone --branch <branchname> <remote-repo-url>

GIT ADD
- git add <nazwa_pliku> <- dodaje plik Stage Selected w source tree
- git add * <- dodaje wszystkie pliki Stage Selected w source tree


Git PUSH
- git push <remote> <branch>
- git push -u origin <nazwa_brancha_na_codespace> <- push brancha na codespace i tam jest tworzony nowy branch

GIT BRANCH
- git branch <- wyświetla dostępne branche
- git branch -a | grep remotes/* <- wyswietli branche na code space
- git branch -r <- wyswietli branche na code space


GIT CHECKOUT
- git checkout <nazwa_brancha> <- przechodzi na dany branch
- git checkout -b <nazwa_brancha> <- tworzy nowy branch z obecnego i ustawia go jako aktywny
- git checkout - <- checkout do ostatniego commita

GIT COMMIT
- git commit -m '<komentarz do commita>'
- git commit --amend -m "Nowy opis" <- zmiana nazwy ostatnigeo commita



GIT REMOVE
git remote rm origin <- usuwa referencje do zdalnego repo

GIT RESET
- git reset --hard <- resetuje brancha
- git reset --hard HEAD~1 resetuje do ostatniego commita (kasuje ostatni commit)

GIT CLEAN
- git clean -fd - kasuje wszystko


GIT SUBMODULE
- git submodule add <adres repo> <nazwa_folderu> (tworzy folder <nazwa_folderu> w miejscu w którym wywołana jest ta komenda
git submodule init
git submodule update



git filter-branch --subdirectory-filter C:/clone_framework/erm-test-automation/framework/erm/steps/ -- --all
git filter-branch --subdirectory-filter subfolder1/subfolder2/FOLDER_TO_KEEP -- --all
